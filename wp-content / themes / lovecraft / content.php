<div id="post-<?php the_ID(); ?>" <?php post_class( 'post' ); ?>>

	<?php
	$post_format = get_post_format() ? get_post_format() : 'standard';
	$post_type = get_post_type();
	?>

	<?php if ( has_post_thumbnail() ) : ?>

		<a class="NoThankYou2020 post-image" title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>#post-<?php the_ID(); ?>">

			<?php
if( get_post_custom_values( 'img150x150' ) !== null ){
	$URLflickr = get_post_custom_values( 'img150x150' )[ 0 ];
}else{	$URLflickr = get_the_post_thumbnail_url() ;
}
if( stripos( $URLflickr , 'flickr.com' ) !== false ){
	$URLflickr = preg_replace('/_.\./','_q.',$URLflickr);
	echo '<img onerror="this.onerror=null;this.width=0;this.height=0;this.src=&#39;//miroir.votre.space/motpressevotrespac/images/Savior.gif&#39;;" src="',$URLflickr,'" alt="" align="left" width="150" height="150" style="margin:0 10px 0 0;" />';
}else{
// the_post_thumbnail( 'post-image' );
$myEyeCatch_attr = array(
	'src'   => $src,
	'class' => "attachment-$size",
	'alt'   => trim( strip_tags( $wp_postmeta->_wp_attachment_image_alt ) )
,	'align'	=> "left"
,	'style'	=> "margin:0 10px 0 0;"
);
the_post_thumbnail( 'thumbnail' , $myEyeCatch_attr );
}
?>

		</a><!-- .featured-media -->

	<?php endif; ?>

	<div class="post-inner">

		<?php if ( $post_format !== 'aside' ) : ?>

			<div class="post-header">

				<?php if ( get_the_title() ) : ?>

					<h2 class="post-title"><div align="right" style="margin-right:10px;"><a href="<?php the_permalink(); ?>#post-<?php the_ID(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></div></h2>

					<?php
				endif;

				if ( is_sticky() ) : ?>

					<a href="<?php the_permalink(); ?>" title="<?php _e( 'Sticky post', 'lovecraft' ) ?>" class="sticky-post">
						<div class="genericon genericon-star"></div>
					</a>

					<?php
				endif;

				if ( $post_type === 'post' ) {
					lovecraft_post_meta();
				}

				?>

			</div><!-- .post-header -->

		<?php endif; ?>

		<?php if ( get_the_content() ) : ?>

			<div class="post-content">
				<?php the_content(); ?>
			</div>

			<div class="clear"></div>

			<?php
		endif;

		if ( $post_type === 'post' && $post_format === 'aside' ) {
			lovecraft_post_meta();
		}

		?>

	</div><!-- .post-inner -->

</div><!-- .post -->
